<?php
use ImmutableStateStatusTracker\Controller\ISSTController;
use ImmutableStateStatusTracker\Factory\ISSTControllerFactory;
return array(
    'service_manager' => array(
        'factories' => array(
            'immutable-state-status-tracker' => '\ImmutableStateStatusTracker\Factory\ImmutableStateStatusFactory'
        )
    ),
    'controllers' => array(
        'factories' => array(
            ISSTController::class => ISSTControllerFactory::class
        )
    ),
    
    'console' => array(
        'router' => array(
            'routes' => array(
                'isst-clean' => array(
                    'type' => 'simple',
                    'options' => array(
                        'route' => 'isst:clean <num_days>',
                        'defaults' => array(
                            '__NAMESPACE__' => 'ImmutableStateStatusTracker\Controller',
                            'controller' => 'ISSTController',
                            'action' => 'clean'
                        )
                    )
                )
            )
        )
    )
);