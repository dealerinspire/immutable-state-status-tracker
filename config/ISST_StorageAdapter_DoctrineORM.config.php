<?php
/**
 * This is a sample configuration for the Doctrine storage adapter and assumes that you installed via composer
 */
return array(
    'doctrine' => array(
        'driver' => array(
            // defines an annotation driver with two paths, and names it `my_annotation_driver`
            'isst_annotation_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                    __DIR__ . '/../../vendor/jackdpeterson/immutable-state-status-tracker/src/ImmutableStateStatusTracker/StorageAdapter/Doctrine'
                )
            ),
            'orm_default' => array(
                'drivers' => array(
                    'ImmutableStateStatusTracker\StorageAdapter\Doctrine' => 'isst_annotation_driver'
                )
            )
        )
    ),
    'immutable-state-status-tracker' => array(
        'storage-adapter' => array(
            'class-name' => '\ImmutableStateStatusTracker\StorageAdapter\DoctrineORM',
            'options' => array(
                'entity_manager' => 'doctrine.entitymanager.orm_default'
            )
        )
    )
);