<?php
return array(
    'immutable-state-status-tracker' => array(
        'storage-adapter' => array(
            'class-name' => '\ImmutableStateStatusTracker\StorageAdapter\Disk',
            'options' => array(
                'path' => __DIR__ . DIRECTORY_SEPARATOR . 'data'
            )
        )
    )
);