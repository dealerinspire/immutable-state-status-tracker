<?php
namespace ImmutableStateStatusTracker\Service;

use ImmutableStateStatusTracker\Entity\Job;
use ImmutableStateStatusTracker\Entity\StatusEvent;
use ImmutableStateStatusTracker\Entity\CalculatedJobStatus;
use ImmutableStateStatusTracker\Exception\InvalidArgumentException;
use ImmutableStateStatusTracker\Exception\StorageAdapterException;
use ImmutableStateStatusTracker\StorageAdapterInterface;
use ImmutableStateStatusTracker\StatusTrackerServiceInterface;
use ImmutableStateStatusTracker\StorageAdapter\AbstractStorageAdapter;
use ImmutableStateStatusTracker\Exception\CalculatingJobStatusException;
use DateTime;

/**
 *
 * @author Jack Peterson
 * @abstract This is a service that is used for tracking complex jobs in a way that eliminates the need for pessimistic write locking to
 *           provide a reliable way to report on status for multi-threaded tasks.
 *          
 *          
 */
final class StatusTracker implements StatusTrackerServiceInterface
{

    protected $_storageAdapter;

    const CACHE_PREFIX = 'ISJI_';

    const CACHE_JOB_KEY = 'job_';

    const CACHE_COMPONENT_KEY = 'comp_';

    const EXC_JOB_MUST_EXIST_MESSAGE = 'A status event cannot be added when to a job that does not exist!';

    const EXC_JOB_MUST_EXIST_CODE = 3;

    const EXC_JOB_MUST_HAVE_AT_LEAST_ONE_COMPONENT_MESSAGE = 'A new job must have at least one component to infer status from';

    const EXC_JOB_MUST_HAVE_AT_LEAST_ONE_COMPONENT_CODE = 4;

    /**
     *
     * @param StorageAdapterInterface $storageAdapter            
     */
    public function __construct(StorageAdapterInterface $storageAdapter)
    {
        $this->_storageAdapter = $storageAdapter;
        // use the construction time as the seed for entropy key seeding
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \ImmutableStateStatusTracker\StatusTrackerServiceInterface::calculateJobStatus()
     */
    public function calculateJobStatus($jobId)
    {
        // TODO Auto-generated method stub
        if (! $this->hasJob($jobId)) {
            throw new \Exception(self::EXC_JOB_MUST_EXIST_MESSAGE, self::EXC_JOB_MUST_EXIST_CODE);
        }
        
        $job = $this->_storageAdapter->fetchJob($jobId);
        
        $statusEvents = $this->_storageAdapter->fetchStatusEvents($job);
        
        if (count($statusEvents) == 0) {
            $calcJobStatusObj = new CalculatedJobStatus($job, CalculatedJobStatus::STATUS_QUEUED, array());
            return $calcJobStatusObj;
        }
        
        $orderedEventsByComponentCollection = array();
        foreach ($job->getComponents() as $componentName) {
            $orderedEventsByComponentCollection[$componentName] = array();
        }
        
        foreach ($statusEvents as $statusEventObj) {
            
            /**
             *
             * @var StatusEvent $statusEventObj
             */
            
            $orderedEventsByComponentCollection[$statusEventObj->getComponentId()][] = $statusEventObj;
        }
        
        // sort the events from earliest to latest
        foreach ($orderedEventsByComponentCollection as $componentName => $statusEventCollection) {
            
            $sortedEventCollection = $statusEventCollection;
            usort($sortedEventCollection, function ($a, $b) {
                /**
                 *
                 * @var StatusEvent $a
                 * @var StatusEvent $b
                 */
                if ($a->getCreatedAt() == $b->getCreatedAt()) {
                    if (floatval($a->getEventId()) < floatval($b->getEventId())) {
                        return - 1;
                    }
                    if (floatval($a->getEventId()) > floatval($b->getEventId())) {
                        return 1;
                    }
                    return 0;
                }
                if ($a->getCreatedAt() < $b->getCreatedAt()) {
                    return - 1;
                }
                if ($a->getCreatedAt() > $b->getCreatedAt()) {
                    return 1;
                }
                
                return 0;
            });
            $orderedEventsByComponentCollection[$componentName] = $sortedEventCollection;
        }
        
        $lastEventCollection = array();
        foreach ($orderedEventsByComponentCollection as $componentName => $statusEventCollection) {
            if (array_key_exists(count($statusEventCollection) - 1, $statusEventCollection)) {
                $lastEventCollection[$componentName] = $statusEventCollection[count($statusEventCollection) - 1];
            }
        }
        
        $calcJobStatusObj = new CalculatedJobStatus($job, $this->calculateOverallJobStatusFromMostRecentEventCollection($job, $lastEventCollection), $lastEventCollection);
        
        return $calcJobStatusObj;
    }

    protected function calculateOverallJobStatusFromMostRecentEventCollection(Job $job, array $mostRecentEventCollection)
    {
        $countQueued = 0;
        $countComplete = 0;
        $countInProgress = 0;
        $countFailed = 0;
        foreach ($mostRecentEventCollection as $statusEvent) {
            /**
             *
             * @var StatusEvent $statusEvent
             */
            switch ($statusEvent->getStatus()) {
                case StatusEvent::STATUS_QUEUED:
                    $countQueued ++;
                    break;
                case StatusEvent::STATUS_IN_PROGRESS:
                    $countInProgress ++;
                    break;
                case StatusEvent::STATUS_COMPLETED:
                    $countComplete ++;
                    break;
                case StatusEvent::STATUS_FAILED:
                    $countFailed ++;
                    break;
                default:
                    $countFailed ++;
            }
        }
        // fail early!
        if ($countFailed >= 1) {
            if ($countFailed == count($job->getComponents()))
                return CalculatedJobStatus::STATUS_FAILED;
            if (($countFailed + $countComplete) == count($job->getComponents()))
                return CalculatedJobStatus::STATUS_FAILED;
            
            return CalculatedJobStatus::STATUS_FAILED_AND_STARTED;
        }
        // if things are only queued but not processing
        if ($countQueued >= 1 && $countInProgress === 0) {
            return CalculatedJobStatus::STATUS_QUEUED;
        }
        
        // if things are in-progress
        if ($countInProgress >= 1 || $countComplete < count($job->getComponents())) {
            return CalculatedJobStatus::STATUS_IN_PROGRESS;
        }
        // if things are complete but not in-progress
        if ($countComplete === count($job->getComponents()) && $countInProgress === 0) {
            return CalculatedJobStatus::STATUS_COMPLETED;
        }
        throw new CalculatingJobStatusException("An edge case was hit when calculating job status.");
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \ImmutableStateStatusTracker\StatusTrackerServiceInterface::hasJob()
     */
    public function hasJob($jobId)
    {
        try {
            if ($this->_storageAdapter->fetchJob($jobId) instanceof Job) {
                return true;
            }
        } Catch (StorageAdapterException $e) {
            if ($e->getCode() == AbstractStorageAdapter::EXC_JOB_NOT_FOUND_CODE) {
                return false;
            }
        }
        return false;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \ImmutableStateStatusTracker\StatusTrackerServiceInterface::createJob()
     */
    public function createJob(array $componentsList)
    {
        if (count($componentsList) < 1) {
            throw new InvalidArgumentException(self::EXC_JOB_MUST_HAVE_AT_LEAST_ONE_COMPONENT_MESSAGE, self::EXC_JOB_MUST_HAVE_AT_LEAST_ONE_COMPONENT_CODE);
        }
        
        $job = $this->_storageAdapter->createJob($componentsList);
        if ($job instanceof Job) {
            return $job;
        } else {
            throw new \Exception("Unable to create the Immutable status job! This should not happen as exceptional conditions should be handled in the storage adapter.");
        }
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \ImmutableStateStatusTracker\StatusTrackerServiceInterface::addStatusEvent()
     */
    public function addStatusEvent($jobId, $componentId, $status, $detailedMessage)
    {
        if (! $this->hasJob($jobId)) {
            throw new \Exception(self::EXC_JOB_MUST_EXIST_MESSAGE, self::EXC_JOB_MUST_EXIST_CODE);
        }
        $job = $this->_storageAdapter->fetchJob($jobId);
        
        $event = $this->_storageAdapter->addStatusEvent($job->getJobId(), $componentId, $status, $detailedMessage);
        
        return $event;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \ImmutableStateStatusTracker\StatusTrackerServiceInterface::removeJob()
     */
    public function removeJob($jobId)
    {
        // TODO Auto-generated method stub
        if ($this->hasJob($jobId)) {
            $job = $this->_storageAdapter->fetchJob($jobId);
            
            $this->_storageAdapter->removeJob($job);
        }
        return true;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \ImmutableStateStatusTracker\StatusTrackerServiceInterface::removeOldJobs()
     */
    public function removeOldJobs(DateTime $dateTimeObj)
    {
        return $this->_storageAdapter->removeOldJobs($dateTimeObj);
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \ImmutableStateStatusTracker\StatusTrackerServiceInterface::fetchAllJobs()
     */
    public function fetchAllJobs($pageNum)
    {
        return $this->_storageAdapter->fetchAllJobs($pageNum);
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \ImmutableStateStatusTracker\StatusTrackerServiceInterface::getStorageAdapter()
     */
    public function getStorageAdapter()
    {
        return $this->_storageAdapter;
    }
}
