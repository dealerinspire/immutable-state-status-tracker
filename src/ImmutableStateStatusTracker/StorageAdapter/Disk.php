<?php
namespace ImmutableStateStatusTracker\StorageAdapter;

use ImmutableStateStatusTracker\StorageAdapterInterface;
use ImmutableStateStatusTracker\Entity\Job;
use ImmutableStateStatusTracker\Entity\StatusEvent;
use ImmutableStateStatusTracker\Exception\StorageAdapterException;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\ArrayAdapter;

/**
 *
 * @author Jack
 * @abstract This adapter would be useful in the following situations: Local testing, single server, and with a shared network mount (S3FS, NFS, Ceph, or GlusterFS).
 * @author Jack D Peterson
 *        
 */
class Disk extends AbstractStorageAdapter implements StorageAdapterInterface
{

    const BAD_PATH_MESSAGE = 'An invalid path was provided: ';

    const BAD_PATH_CODE = 2;

    const PERMISSION_PROBLEM_MESSAGE = 'Unable to write to the path specified. Check permissions. path: ';

    const PERMISSION_PROBLEM_CODE = 3;

    const PATH_MUST_BE_SET_MESSAGE = 'the path must be set in the storage adapter configuration.';

    const PATH_MUST_BE_SET_CODE = 4;

    protected $path;

    protected $config;

    protected $entropyPrefixString;

    public function __construct($config, ServiceLocatorInterface $serviceLocator)
    {
        if (! is_array($config)) {
            throw new StorageAdapterException(self::CONFIGURATION_NOT_ARRAY_MESSAGE, self::CONFIGURATION_NOT_ARRAY_CODE);
        }
        
        $this->config = $config;
        
        if (! array_key_exists('path', $config)) {
            throw new StorageAdapterException(self::PATH_MUST_BE_SET_MESSAGE, self::PATH_MUST_BE_SET_CODE);
        }
        
        if (! is_dir(realpath($config['path']))) {
            throw new StorageAdapterException(self::BAD_PATH_MESSAGE, self::BAD_PATH_CODE);
        }
        $this->path = realpath($config['path']);
        
        $this->entropyPrefixString = substr(hash('sha512', microtime()), 0, 8);
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \ImmutableStateStatusTracker\StorageAdapterInterface::fetchJob()
     */
    public function fetchJob($jobId)
    {
        $path = $this->path . DIRECTORY_SEPARATOR . $jobId . ".json";
        
        if (! file_exists($path)) {
            throw new StorageAdapterException(self::EXC_JOB_NOT_FOUND_MESSAGE, self::EXC_JOB_NOT_FOUND_CODE);
        }
        
        $jobContents = file_get_contents($path);
        
        if (! unserialize($jobContents) instanceof Job) {
            throw new StorageAdapterException(self::EXC_JOB_CANNOT_BE_UNSERIALIZED_MESSAGE, self::EXC_JOB_CANNOT_BE_UNSERIALIZED_CODE);
        } else {
            $job = unserialize($jobContents);
            return $job;
        }
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \ImmutableStateStatusTracker\StorageAdapterInterface::removeAllStatusEvents()
     */
    public function removeAllStatusEvents(Job $job)
    {
        $path = $this->path . DIRECTORY_SEPARATOR . $job->getJobId();
        
        if (is_dir($path)) {
            $events = scandir($path);
            
            foreach ($events as $value) {
                if (is_file($path . DIRECTORY_SEPARATOR . $value)) {
                    unlink($path . DIRECTORY_SEPARATOR . $value);
                }
            }
            rmdir($path);
        }
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \ImmutableStateStatusTracker\StorageAdapterInterface::removeJob()
     */
    public function removeJob(Job $job)
    {
        $this->removeAllStatusEvents($job);
        if (is_file($this->path . DIRECTORY_SEPARATOR . $job->getJobId() . ".json"))
            unlink($this->path . DIRECTORY_SEPARATOR . $job->getJobId() . ".json");
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \ImmutableStateStatusTracker\StorageAdapterInterface::fetchStatusEvents()
     */
    public function fetchStatusEvents(Job $job)
    {
        $events = array();
        $path = $this->path . DIRECTORY_SEPARATOR . $job->getJobId();
        if (is_dir($path)) {
            $potentialEvents = scandir($path);
            foreach ($potentialEvents as $value) {
                if (is_file($path . DIRECTORY_SEPARATOR . $value)) {
                    $potentialEvent = unserialize(file_get_contents($path . DIRECTORY_SEPARATOR . $value));
                    if ($potentialEvent instanceof StatusEvent) {
                        $events[] = $potentialEvent;
                    }
                }
            }
        }
        return $events;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \ImmutableStateStatusTracker\StorageAdapterInterface::createJob()
     */
    public function createJob(array $componentList)
    {
        $jobId = uniqid($this->entropyPrefixString, true);
        
        $job = new Job($jobId, $componentList, time());
        
        $path = $this->path . DIRECTORY_SEPARATOR . $job->getJobId() . ".json";
        
        $out = file_put_contents($path, serialize($job), LOCK_EX);
        
        if ($out === false) {
            throw new StorageAdapterException(self::PERMISSION_PROBLEM_MESSAGE . $path, self::PERMISSION_PROBLEM_CODE);
        }
        return $job;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \ImmutableStateStatusTracker\StorageAdapterInterface::removeOldJobs()
     */
    public function removeOldJobs(\DateTime $dateTimeObj)
    {
        $potentialJobs = scandir($this->path);
        
        foreach ($potentialJobs as $value) {
            if (is_file($this->path . DIRECTORY_SEPARATOR . $value)) {
                
                $job = unserialize(file_get_contents($this->path . DIRECTORY_SEPARATOR . $value));
                if ($job instanceof Job) {
                    $timestamp = $dateTimeObj->getTimestamp();
                    
                    if (intval($job->getCreatedAt()) <= intval($timestamp) || $job->getCreatedAt() == '' || $job->getCreatedAt() == null) {
                        $this->removeJob($job);
                    }
                }
            }
        }
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \ImmutableStateStatusTracker\StorageAdapterInterface::addStatusEvent()
     */
    public function addStatusEvent($jobId, $componentId, $status, $detailedMessage = null)
    {
        $job = $this->fetchJob($jobId);
        
        if (! $job instanceof Job) {
            throw new StorageAdapterException(self::EXC_JOB_NOT_FOUND_MESSAGE, self::EXC_JOB_NOT_FOUND_CODE);
        }
        
        $path = $this->path . DIRECTORY_SEPARATOR . $job->getJobId();
        if (! is_dir($path)) {
            mkdir($path);
        }
        
        $eventId = microtime(true) . "-" . uniqid($this->entropyPrefixString, true);
        
        $event = new StatusEvent($job, $eventId, $componentId, $status, time(), $detailedMessage);
        
        file_put_contents($path . DIRECTORY_SEPARATOR . $eventId . '.json', serialize($event));
        return $event;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \ImmutableStateStatusTracker\StorageAdapterInterface::fetchAllJobs()
     */
    public function fetchAllJobs($pageNum = 1)
    {
        $potentialJobs = scandir($this->path);
        $jobsArr = array();
        $itemsPerPage = 50;
        
        foreach ($potentialJobs as $value) {
            if (is_file($this->path . DIRECTORY_SEPARATOR . $value)) {
                $job = unserialize(file_get_contents($this->path . DIRECTORY_SEPARATOR . $value));
                if ($job instanceof Job) {
                    $jobsArr[] = $job;
                }
            }
        }
        
        $adapter = new ArrayAdapter($jobsArr);
        
        $paginator = new Paginator($adapter);
        $paginator->setItemCountPerPage($itemsPerPage);
        $paginator->setCurrentPageNumber($pageNum);
        return $paginator;
    }
}