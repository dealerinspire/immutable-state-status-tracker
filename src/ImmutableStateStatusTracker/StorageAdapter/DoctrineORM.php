<?php
namespace ImmutableStateStatusTracker\StorageAdapter;

use ImmutableStateStatusTracker\Entity\Job;
use ImmutableStateStatusTracker\StorageAdapter\Doctrine\Job as DoctrineJob;
use ImmutableStateStatusTracker\StorageAdapter\Doctrine\StatusEvent as DoctrineStatusEvent;
use DateTime;
use ImmutableStateStatusTracker\Entity\StatusEvent;
use ImmutableStateStatusTracker\Exception\StorageAdapterException;
use Doctrine\ORM\EntityManagerInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Paginator\Paginator;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;
use Doctrine\ORM\QueryBuilder;
use Zend\Paginator\Adapter\Callback;

class DoctrineORM extends AbstractStorageAdapter
{

    protected $_em;

    const EXC_NOT_A_DOCTRINE_ENTITY_MANAGER_MESSAGE = 'Doctrine ORM adapter for ISST did not get an actual entity manager as defined by the configuration.';

    const EXC_NOT_A_DOCTRINE_ENTITY_MANAGER_CODE = 5;

    const EXC_ENTITY_MANAGER_NOT_FOUND_MESSAGE = 'The entity manager was not found with the provided name [$serviceManager->has(\'example\')].';

    const EXC_ENTITY_MANAGER_NOT_FOUND_CODE = 6;

    const EXC_ENTITY_MANAGER_NOT_DEFINED_IN_CONFIG_MESSAGE = 'The entity_manager was not defined in the configuration.';

    const EXC_ENTITY_MANAGER_NOT_DEFINED_IN_CONFIG_CODE = 7;

    public function __construct($config, ServiceLocatorInterface $serviceLocator)
    {
        if (! is_array($config)) {
            throw new StorageAdapterException(self::CONFIGURATION_NOT_ARRAY_MESSAGE, self::CONFIGURATION_NOT_ARRAY_CODE);
        }
        
        $this->config = $config;
        
        if (! array_key_exists('entity_manager', $config)) {
            throw new StorageAdapterException(self::EXC_ENTITY_MANAGER_NOT_DEFINED_IN_CONFIG_MESSAGE, self::EXC_ENTITY_MANAGER_NOT_DEFINED_IN_CONFIG_CODE);
        }
        $entity_manager_name = $config['entity_manager'];
        
        if (! $serviceLocator->has($entity_manager_name)) {
            throw new StorageAdapterException(self::EXC_ENTITY_MANAGER_NOT_FOUND_MESSAGE, self::EXC_ENTITY_MANAGER_NOT_FOUND_CODE);
        }
        $em = $serviceLocator->get($entity_manager_name);
        
        if (! $em instanceof EntityManagerInterface) {
            throw new StorageAdapterException(self::EXC_NOT_A_DOCTRINE_ENTITY_MANAGER_MESSAGE, self::EXC_NOT_A_DOCTRINE_ENTITY_MANAGER_CODE);
        }
        
        $this->_em = $em;
    }

    public function fetchJob($jobId)
    {
        $dJob = $this->_em->find('\ImmutableStateStatusTracker\StorageAdapter\Doctrine\Job', $jobId);
        if (! $dJob instanceof DoctrineJob) {
            return false;
        } else {
            $job = new Job($jobId, $dJob->getComponents(), $dJob->getCreatedAt()->getTimestamp());
            return $job;
        }
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \ImmutableStateStatusTracker\StorageAdapterInterface::removeAllStatusEvents()
     */
    public function removeAllStatusEvents(Job $job)
    {
        
        /**
         *
         * @var Job
         */
        $dJob = $this->_em->find('\ImmutableStateStatusTracker\StorageAdapter\Doctrine\Job', $job->getJobId());
        if (! $job instanceof Job) {
            throw new StorageAdapterException(self::EXC_JOB_NOT_FOUND_MESSAGE, self::EXC_JOB_NOT_FOUND_CODE);
        }
        
        $qb = $this->_em->createQueryBuilder();
        $qb->delete('\ImmutableStateStatusTracker\StorageAdapter\Doctrine\StatusEvent', 'se');
        $qb->where('se._job = :job');
        $qb->setParameter('job', $dJob);
        $qb->getQuery()->execute();
        
        $this->_em->clear('\ImmutableStateStatusTracker\StorageAdapter\Doctrine\StatusEvent');
        $_statusEvents = array();
        $dJob->setStatusEvents($_statusEvents);
        $this->_em->persist($dJob);
        $this->_em->flush();
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \ImmutableStateStatusTracker\StorageAdapterInterface::removeJob()
     */
    public function removeJob(Job $job)
    {
        // cascade = remove should make this irrelevant
        // $this->removeAllStatusEvents($job);
        $dJob = $this->_em->find('\ImmutableStateStatusTracker\StorageAdapter\Doctrine\Job', $job->getJobId());
        if (! $job instanceof Job) {
            throw new StorageAdapterException(self::EXC_JOB_NOT_FOUND_MESSAGE, self::EXC_JOB_NOT_FOUND_CODE);
        }
        
        $this->_em->remove($dJob);
        $this->_em->flush();
        return true;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \ImmutableStateStatusTracker\StorageAdapterInterface::createJob()
     */
    public function createJob(array $componentsList)
    {
        $dJob = new DoctrineJob();
        $dJob->setComponents($componentsList);
        $dJob->setCreatedAt(new \DateTime());
        $this->_em->persist($dJob);
        $this->_em->flush();
        
        $job = new Job($dJob->getJobId(), $dJob->getComponents(), $dJob->getCreatedAt()->getTimestamp());
        
        return $job;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \ImmutableStateStatusTracker\StorageAdapterInterface::fetchStatusEvents()
     */
    public function fetchStatusEvents(Job $job)
    {
        $dJob = $this->_em->find('\ImmutableStateStatusTracker\StorageAdapter\Doctrine\Job', $job->getJobId());
        if (! $job instanceof Job) {
            throw new StorageAdapterException(self::EXC_JOB_NOT_FOUND_MESSAGE, self::EXC_JOB_NOT_FOUND_CODE);
        }
        
        $results = $this->_em->getRepository('\ImmutableStateStatusTracker\StorageAdapter\Doctrine\StatusEvent')->findBy(array(
            '_job' => $dJob
        ));
        
        $out = array();
        
        /**
         *
         * @var DoctrineStatusEvent $dStatusEvent
         */
        foreach ($results as $dStatusEvent) {
            $statusEvent = new StatusEvent($job, $dStatusEvent->getStatusId(), $dStatusEvent->getComponentId(), $dStatusEvent->getStatus(), $dStatusEvent->getCreatedAt(), $dStatusEvent->getDetailedMessage());
            $out[] = $statusEvent;
        }
        
        return $out;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \ImmutableStateStatusTracker\StorageAdapterInterface::removeOldJobs()
     */
    public function removeOldJobs(DateTime $dateTimeObj)
    {
        try {
            
            $qb = $this->_em->createQueryBuilder();
            $qb->delete('\ImmutableStateStatusTracker\StorageAdapter\Doctrine\Job', 'dj');
            $qb->where('dj.createdAt <= :createdAt');
            $qb->setParameter('createdAt', $dateTimeObj);
            
            $qb->getQuery()->execute();
        } catch (\Exception $e) {
            echo $e->getMessage();
            echo $e->getPrevious()->getMessage();
        }
        $this->_em->clear();
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \ImmutableStateStatusTracker\StorageAdapterInterface::addStatusEvent()
     */
    public function addStatusEvent($jobId, $componentId, $status, $detailedMessage = null)
    {
        $dJob = $this->_em->find('\ImmutableStateStatusTracker\StorageAdapter\Doctrine\Job', $jobId);
        if (! $dJob instanceof DoctrineJob) {
            throw new StorageAdapterException(self::EXC_JOB_NOT_FOUND_MESSAGE, self::EXC_JOB_NOT_FOUND_CODE);
        }
        
        $job = $this->fetchJob($jobId);
        
        $dStatusEvent = new DoctrineStatusEvent();
        $dStatusEvent->setCreatedAt(new DateTime());
        $dStatusEvent->setComponentId($componentId);
        $dStatusEvent->setDetailedMessage($detailedMessage);
        $dStatusEvent->setJob($dJob);
        $dStatusEvent->setStatus($status);
        $dJob->getStatusEvents()->add($dStatusEvent);
        $this->_em->persist($dStatusEvent);
        $this->_em->flush();
        
        $statusEvent = new StatusEvent($job, $dStatusEvent->getStatusId(), $componentId, $status, $dStatusEvent->getCreatedAt(), $detailedMessage);
        
        return $statusEvent;
    }

    /**
     *
     * {@inheritdoc}
     *
     * @see \ImmutableStateStatusTracker\StorageAdapterInterface::fetchAllJobs()
     */
    public function fetchAllJobs($pageNum = 1)
    {
        
        /**
         *
         * @var QueryBuilder $qb
         */
        $qb = $this->_em->createQueryBuilder();
        $query = $qb->select('dj')->from('\ImmutableStateStatusTracker\StorageAdapter\Doctrine\Job', 'dj');
        // $qb->setHydrationMode('\Doctrine\ORM\Query::HYDRATE_ARRAY');
        $adapter = new DoctrineAdapter(new ORMPaginator($query));
        $paginator = new Paginator($adapter);
        $paginator->setItemCountPerPage(50);
        $callBackAdapter = new Callback(function ($offset, $itemCountPerPage) use ($paginator) {
            $arrOut = array();
            foreach ($paginator->getItemsByPage($offset) as $obj) {
                $arrOut[] = new Job($obj->getJobId(), $obj->getComponents(), $obj->getCreatedAt());
            }
            return $arrOut;
        }, function () use ($paginator) {
            return $paginator->getTotalItemCount();
        });
        
        $jobPaginator = new Paginator($callBackAdapter);
        
        $jobPaginator->setItemCountPerPage(50);
        return $jobPaginator;
    }
}
