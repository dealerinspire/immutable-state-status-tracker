<?php
namespace ImmutableStateStatusTracker\StorageAdapter\Doctrine;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity()
 * @ORM\Table(name="isst_job")
 *
 * @author Jack
 *        
 */
final class Job
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="job_id", type="integer")
     */
    protected $jobId;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="array")
     */
    protected $components;

    /**
     * @ORM\OneToMany(targetEntity="ImmutableStateStatusTracker\StorageAdapter\Doctrine\StatusEvent", mappedBy="_job", cascade={"remove"})
     */
    protected $_statusEvents;

    public function __construct()
    {
        $this->components = array();
        $this->_statusEvents = new ArrayCollection();
    }

    /**
     *
     * @return the $jobId
     */
    public final function getJobId()
    {
        return $this->jobId;
    }

    /**
     *
     * @return the $createdAt
     */
    public final function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     *
     * @return the $components
     */
    public final function getComponents()
    {
        return $this->components;
    }

    /**
     *
     * @return the $_statusEvents
     */
    public final function getStatusEvents()
    {
        return $this->_statusEvents;
    }

    /**
     *
     * @param field_type $_statusEvents            
     */
    public final function setStatusEvents($_statusEvents)
    {
        $this->_statusEvents = $_statusEvents;
    }

    /**
     *
     * @param field_type $createdAt            
     */
    public final function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     *
     * @param field_type $components            
     */
    public final function setComponents($components)
    {
        $this->components = $components;
    }
}