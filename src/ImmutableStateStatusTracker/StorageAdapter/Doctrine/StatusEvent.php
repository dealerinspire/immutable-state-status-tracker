<?php
namespace ImmutableStateStatusTracker\StorageAdapter\Doctrine;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="isst_status_event")
 *
 * @author Jack
 *        
 */
final class StatusEvent
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $statusId;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="ImmutableStateStatusTracker\StorageAdapter\Doctrine\Job", inversedBy="_statusEvents")
     * @ORM\JoinColumn(name="job_id", referencedColumnName="job_id", onDelete="CASCADE")
     */
    protected $_job;

    /**
     * @ORM\Column(type="string")
     */
    protected $componentId;

    /**
     * @ORM\Column(type="string")
     *
     * @var unknown
     */
    protected $status;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @var unknown
     */
    protected $detailedMessage;

    /**
     *
     * @return the $createdAt
     */
    public final function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     *
     * @return the $componentId
     */
    public final function getComponentId()
    {
        return $this->componentId;
    }

    /**
     *
     * @return the $status
     */
    public final function getStatus()
    {
        return $this->status;
    }

    /**
     *
     * @return the $detailedMessage
     */
    public final function getDetailedMessage()
    {
        return $this->detailedMessage;
    }

    /**
     *
     * @param field_type $createdAt            
     */
    public final function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     *
     * @return the $statusId
     */
    public final function getStatusId()
    {
        return $this->statusId;
    }

    /**
     *
     * @return the $_job
     */
    public final function getJob()
    {
        return $this->_job;
    }

    /**
     *
     * @param field_type $_job            
     */
    public final function setJob($_job)
    {
        $this->_job = $_job;
    }

    /**
     *
     * @param field_type $componentId            
     */
    public final function setComponentId($componentId)
    {
        $this->componentId = $componentId;
    }

    /**
     *
     * @param field_type $status            
     */
    public final function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     *
     * @param field_type $detailedMessage            
     */
    public final function setDetailedMessage($detailedMessage)
    {
        $this->detailedMessage = $detailedMessage;
    }
}