<?php
namespace ImmutableStateStatusTracker\StorageAdapter;

use ImmutableStateStatusTracker\StorageAdapterInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

abstract class AbstractStorageAdapter implements StorageAdapterInterface, ServiceLocatorAwareInterface
{

    const EXC_JOB_NOT_FOUND_MESSAGE = 'No job exists with the ID provided.';

    const EXC_JOB_NOT_FOUND_CODE = 404;

    const EXC_JOB_CANNOT_BE_UNSERIALIZED_CODE = 510;

    const EXC_JOB_CANNOT_BE_UNSERIALIZED_MESSAGE = 'The job cannot be unserialized. The data may be corrupt.';

    const CONFIGURATION_NOT_ARRAY_MESSAGE = 'the provided configuration variable was not an array!';

    const CONFIGURATION_NOT_ARRAY_CODE = 4;

    protected $serviceLocator;

    /**
     * Set service locator
     *
     * @param ServiceLocatorInterface $serviceLocator            
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Get service locator
     *
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}