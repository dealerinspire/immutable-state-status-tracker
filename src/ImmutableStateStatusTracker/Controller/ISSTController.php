<?php
namespace ImmutableStateStatusTracker\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use ImmutableStateStatusTracker\Service\StatusTracker;

class ISSTController extends AbstractActionController
{

    protected $service;

    public function __construct(StatusTracker $statusTracker)
    {
        $this->service = $statusTracker;
    }

    public function cleanAction()
    {
        $numDaysAgo = $this->getEvent()
            ->getRequest()
            ->getParam('num_days');
        
        if (abs(intval($numDaysAgo)) == 0) {
            echo "Failed! Must have days >= 1\n";
            exit(2);
        }
        
        $date = new \DateTime();
        $date->sub(new \DateInterval('P' . abs((int) $numDaysAgo) . 'D'));
        
        $this->service->removeOldJobs($date);
        echo "Done cleaning.";
    }
}