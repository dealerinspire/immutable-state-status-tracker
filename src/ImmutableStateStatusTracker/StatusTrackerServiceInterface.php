<?php
namespace ImmutableStateStatusTracker;

use ImmutableStateStatusTracker\Entity\CalculatedJobStatus;
use ImmutableStateStatusTracker\Entity\Job;
use ImmutableStateStatusTracker\Entity\StatusEvent;
use DateTime;
use Zend\Paginator\Paginator;

interface StatusTrackerServiceInterface
{

    /**
     *
     * @abstract Create a new job for tracking purposes. You must know what components are to be tracked in advance.
     * @param array $componentsList            
     * @return Job
     */
    public function createJob(array $componentsList);

    /**
     *
     * @abstract This is the primary function of this class. calculateJobStatus determines the status at this moment given presently available information.
     * @param string $jobId            
     * @return CalculatedJobStatus
     */
    public function calculateJobStatus($jobId);

    /**
     *
     * @abstract determine if a job with the provided job ID exists.
     * @param string $jobId            
     * @return boolean
     */
    public function hasJob($jobId);

    /**
     *
     * @param string $jobId            
     * @param string $componentId            
     * @param string $status            
     * @param string $detailedMessage            
     * @return StatusEvent
     */
    public function addStatusEvent($jobId, $componentId, $status, $detailedMessage);

    /**
     *
     * @abstract removes a job and underlying status events from cache
     * @param string $jobId            
     * @return void
     */
    public function removeJob($jobId);

    /**
     *
     * @abstract remove jobs that have a createdAt time that is equal to or less than the timestamp provided
     * @param DateTime $dateTimeObj            
     * @return void
     */
    public function removeOldJobs(DateTime $dateTimeObj);

    /**
     *
     * @abstract Return a Paginator instance of the jobs
     * @param int $pageNum            
     * @return Paginator
     */
    public function fetchAllJobs($pageNum);

    /**
     *
     * @abstract Generally useful if you need to perform a specific action against a storage adapter.
     * @return StorageAdapterInterface
     */
    public function getStorageAdapter();
}