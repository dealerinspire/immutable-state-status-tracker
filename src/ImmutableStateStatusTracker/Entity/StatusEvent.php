<?php
namespace ImmutableStateStatusTracker\Entity;

use ImmutableStateStatusTracker\Entity\Job;
use ImmutableStateStatusTracker\Exception\InvalidArgumentException;

final class StatusEvent
{

    protected $eventId;

    protected $createdAt;

    protected $jobId;

    protected $componentId;

    protected $status;

    protected $detailedMessage;

    const EXC_INVALID_STATUS_PROVIDED_MESSAGE = 'an invalid status was provided.';

    const EXC_INVALID_STATUS_PROVIDED_CODE = 2;

    const EXC_INVALID_COMPONENT_ID_PROVIDED_CODE = 3;

    const EXC_INVALID_COMPONENT_ID_PROVDED_MESSAGE = 'An invalid component id was provided. Status event will not be created.';

    const STATUS_QUEUED = 'se_queued';

    const STATUS_IN_PROGRESS = 'se_in_progress';

    const STATUS_FAILED = 'se_failed';

    const STATUS_COMPLETED = 'se_completed';

    /**
     *
     * @param Job $job            
     * @param string $componentId            
     * @param string $status            
     * @param string $createdAt            
     * @param string $detailedMessage            
     * @throws InvalidArgumentException
     */
    public function __construct(Job $job, $eventId, $componentId, $status, $createdAt, $detailedMessage)
    {
        $this->jobId = $job->getJobId();
        
        if (! in_array($componentId, $job->getComponents())) {
            throw new InvalidArgumentException(self::EXC_INVALID_COMPONENT_ID_PROVDED_MESSAGE, self::EXC_INVALID_COMPONENT_ID_PROVIDED_CODE);
        }
        $this->componentId = $componentId;
        
        $validStatusArr = array(
            0 => self::STATUS_QUEUED,
            1 => self::STATUS_IN_PROGRESS,
            2 => self::STATUS_FAILED,
            3 => self::STATUS_COMPLETED
        );
        
        if (! in_array($status, $validStatusArr)) {
            throw new InvalidArgumentException(self::EXC_INVALID_STATUS_PROVIDED_MESSAGE, self::EXC_INVALID_STATUS_PROVIDED_CODE);
        }
        
        $this->status = $status;
        
        $this->detailedMessage = (string) $detailedMessage;
        
        $this->createdAt = $createdAt;
        
        $this->eventId = $eventId;
    }

    /**
     *
     * @return the $eventId
     */
    public final function getEventId()
    {
        return $this->eventId;
    }

    /**
     *
     * @return the $createdAt
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     *
     * @return the $jobId
     */
    public function getJobId()
    {
        return $this->jobId;
    }

    /**
     *
     * @return the $componentId
     */
    public function getComponentId()
    {
        return $this->componentId;
    }

    /**
     *
     * @return the $status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     *
     * @return the $detailedMessage
     */
    public function getDetailedMessage()
    {
        return $this->detailedMessage;
    }

    public function toArray()
    {
        return get_object_vars($this);
    }
}