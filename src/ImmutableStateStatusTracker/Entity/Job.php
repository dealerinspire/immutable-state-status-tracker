<?php
namespace ImmutableStateStatusTracker\Entity;

final class Job
{

    const COMPONENTS_NOT_ARRAY_MESSAGE = 'The components parameter provided in the ImmutableJobStatus entity was not an array.';

    const COMPONENTS_NOT_ARRAY_CODE = 2;

    protected $jobId;

    protected $createdAt;

    protected $components;

    public function __construct($jobId, $components, $createdAt)
    {
        $this->jobId = (string) $jobId;
        
        $this->components = $components;
        
        $this->createdAt = $createdAt;
    }

    /**
     *
     * @return the $createdAt
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     *
     * @return the $jobId
     */
    public function getJobId()
    {
        return $this->jobId;
    }

    /**
     *
     * @return the $components
     */
    public function getComponents()
    {
        return $this->components;
    }

    public function toArray()
    {
        return get_object_vars($this);
    }
}