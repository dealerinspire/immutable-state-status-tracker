<?php
namespace ImmutableStateStatusTracker\Entity;

use ImmutableStateStatusTracker\Exception\InvalidArgumentException;
use ImmutableStateStatusTracker\Entity\Job;

final class CalculatedJobStatus
{

    /**
     *
     * @var Job
     */
    protected $job;

    protected $overallStatus;

    protected $mostRecentEventCollection;

    const STATUS_QUEUED = 's_calc_queued';

    const STATUS_IN_PROGRESS = 's_calc_in_progress';

    const STATUS_COMPLETED = 's_calc_completed';

    const STATUS_FAILED = 's_calc_failed';

    const STATUS_FAILED_AND_STARTED = 's_calc_failed_in_progress';

    const EXC_INVALID_OVERALL_STATUS_SUPPLIED_MESSAGE = 'An invalid overall status was provided';

    const EXC_INVALID_OVERALL_STATUS_SUPPLIED_CODE = 2;

    /**
     *
     * @return \ImmutableStateStatusTracker\Entity\Job
     */
    public final function getJob()
    {
        return $this->job;
    }

    /**
     *
     * @return string $overallStatus
     */
    public final function getOverallStatus()
    {
        return $this->overallStatus;
    }

    /**
     *
     * @return the $mostRecentEventCollection
     */
    public final function getMostRecentEventCollection()
    {
        return $this->mostRecentEventCollection;
    }

    public function __construct(Job $job, $overallStatus, array $mostRecentEventCollection)
    {
        $this->job = $job;
        
        $validStatusArr = array(
            self::STATUS_QUEUED,
            self::STATUS_IN_PROGRESS,
            self::STATUS_COMPLETED,
            self::STATUS_FAILED,
            self::STATUS_FAILED_AND_STARTED
        );
        
        if (! in_array($overallStatus, $validStatusArr)) {
            throw new InvalidArgumentException(self::EXC_INVALID_OVERALL_STATUS_SUPPLIED_MESSAGE, self::EXC_INVALID_OVERALL_STATUS_SUPPLIED_CODE);
        }
        
        $this->overallStatus = $overallStatus;
        $this->mostRecentEventCollection = $mostRecentEventCollection;
    }

    public function toArray()
    {
        $out = array(
            'job' => $this->job->toArray(),
            'mostRecentEventCollection' => array(),
            'overallStatus' => $this->getOverallStatus()
        );
        
        foreach ($this->mostRecentEventCollection as $key => $statusEvent) {
            $out['mostRecentEventCollection'][$key] = $statusEvent->toArray();
        }
        
        return $out;
    }
}