<?php
namespace ImmutableStateStatusTracker\Exception;

use Exception;

final class InvalidArgumentException extends Exception
{
}
