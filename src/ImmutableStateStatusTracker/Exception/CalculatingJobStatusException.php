<?php
namespace ImmutableStateStatusTracker\Exception;

use Exception;

class CalculatingJobStatusException extends Exception
{
}