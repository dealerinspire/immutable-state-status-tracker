<?php
namespace ImmutableStateStatusTracker\Exception;

use Exception;

final class StorageAdapterException extends Exception
{
}