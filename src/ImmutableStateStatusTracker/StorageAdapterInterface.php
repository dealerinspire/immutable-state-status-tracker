<?php
namespace ImmutableStateStatusTracker;

use ImmutableStateStatusTracker\Entity\Job;
use ImmutableStateStatusTracker\Entity\StatusEvent;
use ImmutableStateStatusTracker\StorageAdapter\StorageAdapterException;
use Zend\Paginator\Paginator;

interface StorageAdapterInterface
{

    /**
     *
     * @param string $jobId            
     * @return Job
     * @throws StorageAdapterException
     * @abstract fetch a job from the underlying storage adapter
     */
    public function fetchJob($jobId);

    /**
     *
     * @param Job $job            
     * @return void
     * @abstract remove an individual job for cleanup purposes
     */
    public function removeJob(Job $job);

    /**
     *
     * @param array $componentList            
     * @return Job
     * @abstract persist a job into the underlying storage adapter
     */
    public function createJob(array $componentList);

    /**
     *
     * @param Job $job            
     * @return array array(int => StatusEvent )
     * @abstract fetch ALL events for the job from the underlying storage adapter
     */
    public function fetchStatusEvents(Job $job);

    /**
     *
     * @param Job $job            
     * @return void
     * @abstract clean up all status events (reset to queued state)
     */
    public function removeAllStatusEvents(Job $job);

    /**
     *
     * @param string $jobId            
     * @param string $componentId            
     * @param string $status            
     * @param string $detailedMessage            
     * @return StatusEvent
     */
    public function addStatusEvent($jobId, $componentId, $status, $detailedMessage = null);

    /**
     *
     * @param \DateTime $dateTimeObj            
     * @return void
     * @abstract periodically run this to clean up old jobs and underlying events.
     */
    public function removeOldJobs(\DateTime $dateTimeObj);

    /**
     *
     * @abstract return an Paginator of Job objects. Warning: this could be huge!
     * @param int $pageNum            
     * @return Paginator
     */
    public function fetchAllJobs($pageNum = 1);
}