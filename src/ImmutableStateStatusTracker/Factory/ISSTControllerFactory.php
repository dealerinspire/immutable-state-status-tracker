<?php
namespace ImmutableStateStatusTracker\Factory;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\FactoryInterface;
use ImmutableStateStatusTracker\Controller\ISSTController;

class ISSTControllerFactory
{

    /**
     *
     * {@inheritDoc}
     *
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function __invoke(\Zend\ServiceManager\ServiceLocatorInterface $serviceLocator)
    {
        try {
            $parentLocator = $serviceLocator->getServiceLocator();
            $service = $parentLocator->get('immutable-state-status-tracker');
            
            $controller = new ISSTController($service);
            
            return $controller;
        } catch (\Exception $e) {
            syslog(LOG_ERR, "ISSTControllerFactory error: " . $e->getMessage() . "\n" . $e->getTraceAsString());
        }
    }
}