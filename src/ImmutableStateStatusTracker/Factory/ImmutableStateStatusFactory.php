<?php
namespace ImmutableStateStatusTracker\Factory;

use Zend\ServiceManager\ServiceLocatorInterface;
use ImmutableStateStatusTracker\Service\StatusTracker;
use ImmutableStateStatusTracker\Exception\ConfigurationException;
use Zend\ServiceManager\FactoryInterface;

class ImmutableStateStatusFactory
{

    const NO_CONFIGURATION_FOUND_MESSAGE = 'No configuration information was found to spin up the immutable-state-status-tracker module.';

    const STORAGE_ADAPTER_NOT_DEFINED_MESSAGE = 'the storage adapter has not been defined!';

    const STORAGE_ADAPTER_CLASS_NAME_NOT_DEFINED_MESSAGE = 'the storage adapter class name must be specified and resolvable.';

    const STORAGE_ADAPTER_CLASS_NAME_NOT_EXIST = 'The class provided as a storage adapter does not exist or was not autolaoded properly.';

    const STORAGE_ADAPTER_OPTIONS_NOT_PRESENT = 'The storage adapter options key must be present and in the form of an array';

    /**
     *
     * {@inheritDoc}
     *
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function __invoke(\Zend\ServiceManager\ServiceLocatorInterface $serviceLocator)
    {
        try {
            $globalConfig = $serviceLocator->get('Config');
            
            if (! array_key_exists('immutable-state-status-tracker', $globalConfig)) {
                throw new ConfigurationException(self::NO_CONFIGURATION_FOUND_MESSAGE);
            }
            $options = $globalConfig['immutable-state-status-tracker'];
            if (! array_key_exists('storage-adapter', $options)) {
                throw new ConfigurationException(self::STORAGE_ADAPTER_NOT_DEFINED_MESSAGE);
            }
            
            if (! array_key_exists('class-name', $options['storage-adapter'])) {
                throw new ConfigurationException(self::STORAGE_ADAPTER_CLASS_NAME_NOT_DEFINED_MESSAGE);
            }
            $classToLoad = $options['storage-adapter']['class-name'];
            if (! class_exists($options['storage-adapter']['class-name'])) {
                throw new ConfigurationException(self::STORAGE_ADAPTER_CLASS_NAME_NOT_EXIST);
            }
            
            if (! array_key_exists('options', $options['storage-adapter'])) {
                throw new ConfigurationException(self::STORAGE_ADAPTER_OPTIONS_NOT_PRESENT);
            }
            
            $storageAdapter = new $classToLoad($options['storage-adapter']['options'], $serviceLocator);
            
            $service = new StatusTracker($storageAdapter);
            return $service;
        } catch (\Exception $e) {
            syslog(LOG_ERR, "ImmutableStateStatusTracker Factory error: " . $e->getMessage() . "\n" . $e->getTraceAsString());
        }
    }
}