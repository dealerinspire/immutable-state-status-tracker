<?php
namespace ImmutableStateStatusTracker;

use Zend\Loader\AutoloaderFactory;
use Zend\Mvc\Service\ServiceManagerConfig;
use Zend\ServiceManager\ServiceManager;
error_reporting(E_ALL | E_STRICT);
chdir(__DIR__);

if (! defined('APPLICATION_ENV')) {
    define('APPLICATION_ENV', 'development');
}

if (! defined('APPLICATION_PATH')) {
    
    // in the modules folder
    if ((strpos(__DIR__, 'module') > 1 ? true : false) && is_dir(realpath(__DIR__ . '/../../../'))) {
        define('APPLICATION_PATH', realpath(__DIR__ . '/../../../'));
    }
    // in the vendor/jackdpeterson/immutable-state-status-tracker folder
    if ((strpos(__DIR__, 'vendor') > 1 ? true : false) && is_dir(realpath(__DIR__ . '/../../../../'))) {
        define('APPLICATION_PATH', realpath(__DIR__ . '/../../../../'));
    }
}

/**
 * Test bootstrap, for setting up autoloading
 */
class Bootstrap
{

    protected static $serviceManager;

    public static function init()
    {
        $zf2ModulePaths = array(
            dirname(dirname(__DIR__))
        );
        
        $path = self::findParentPath('vendor');
        if (($path)) {
            $zf2ModulePaths[] = $path;
        }
        if (($path = static::findParentPath('module')) !== $zf2ModulePaths[0]) {
            $zf2ModulePaths[] = $path;
        }
        
        self::initAutoloader();
        
        // use ModuleManager to load this module and it's dependencies
        $config = array(
            'module_listener_options' => array(
                'config_glob_paths' => array(
                    sprintf(APPLICATION_PATH . '/config/autoload/{,*.}{global,%s,local}.php', APPLICATION_ENV)
                ),
                'module_paths' => array(
                    APPLICATION_PATH . '/module',
                    APPLICATION_PATH . '/vendor'
                )
            ),
            'modules' => array('ImmutableStateStatusTracker')
        );
        
        $serviceManager = new ServiceManager(new ServiceManagerConfig());
        $serviceManager->setService('ApplicationConfig', $config);
        $serviceManager->get('ModuleManager')->loadModules();
        self::$serviceManager = $serviceManager;
    }

    public static function chroot()
    {
        $rootPath = dirname(static::findParentPath('module'));
        chdir($rootPath);
    }

    public static function getServiceManager()
    {
        return self::$serviceManager;
    }

    protected static function initAutoloader()
    {
        $vendorPath = static::findParentPath('vendor');
        
        $zf2Path = getenv('ZF2_PATH');
        if (! $zf2Path) {
            if (defined('ZF2_PATH')) {
                $zf2Path = ZF2_PATH;
            } elseif (is_dir($vendorPath . '/ZF2/library')) {
                $zf2Path = $vendorPath . '/ZF2/library';
            } elseif (is_dir($vendorPath . '/zendframework')) {
                $zf2Path = $vendorPath . '/zendframework';
            }
        }
        
        if (! $zf2Path) {
            throw new \RuntimeException('Unable to load ZF2. Run `php composer.phar install` or' . ' define a ZF2_PATH environment variable.');
        }
        
        if (file_exists($vendorPath . '/autoload.php')) {
            include $vendorPath . '/autoload.php';
        }
        
        include $zf2Path . '/zend-Loader/src/AutoloaderFactory.php';
        AutoloaderFactory::factory(array(
            'Zend\Loader\StandardAutoloader' => array(
                'autoregister_zf' => true,
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/' . __NAMESPACE__
                )
            )
        ));
    }

    protected static function findParentPath($path)
    {
        $dir = __DIR__;
        $previousDir = '.';
        while (! is_dir($dir . '/' . $path)) {
            $dir = dirname($dir);
            if ($previousDir === $dir) {
                return false;
            }
            $previousDir = $dir;
        }
        return $dir . '/' . $path;
    }
}

Bootstrap::init();
Bootstrap::chroot();
