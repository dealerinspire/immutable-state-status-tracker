<?php
require_once __DIR__ . '/../../../src/ImmutableStateStatusTracker/Exception/StorageAdapterException.php';
require_once __DIR__ . '/../../../src/ImmutableStateStatusTracker/StorageAdapterInterface.php';
require_once __DIR__ . '/../../../src/ImmutableStateStatusTracker/Entity/Job.php';
require_once __DIR__ . '/../../../src/ImmutableStateStatusTracker/Entity/StatusEvent.php';
require_once __DIR__ . '/../../../src/ImmutableStateStatusTracker/StorageAdapter/AbstractStorageAdapter.php';
require_once __DIR__ . '/../../../src/ImmutableStateStatusTracker/StorageAdapter/DoctrineORM.php';

use ImmutableStateStatusTracker\Bootstrap;
use ImmutableStateStatusTracker\StorageAdapter\DoctrineORM;
use ImmutableStateStatusTracker\Entity\StatusEvent;

/**
 * DoctrineORM test case.
 * @group doctrine
 */
class DoctrineORMTest extends PHPUnit_Framework_TestCase
{

    /**
     *
     * @var DoctrineORM
     */
    private $doctrineORM;

    public function getServiceManager()
    {
        return Bootstrap::getServiceManager();
    }

    /**
     * Prepares the environment before running a test.
     */
    protected function setUp()
    {
        parent::setUp();
        
        $this->doctrineORM = new DoctrineORM(array(
            'entity_manager' => 'doctrine.entitymanager.orm_default'
        ), $this->getServiceManager());
        
        $this->assertInstanceOf('\ImmutableStateStatusTracker\StorageAdapterInterface', $this->doctrineORM);
    }

    /**
     * Cleans up the environment after running a test.
     */
    protected function tearDown()
    {
        // TODO Auto-generated DoctrineORMTest::tearDown()
        $this->doctrineORM = null;
        
        parent::tearDown();
    }

    /**
     * Constructs the test case.
     */
    public function __construct()
    {
        // TODO Auto-generated constructor
    }

    /**
     * Tests DoctrineORM->__construct()
     */
    public function test__construct_MISSING_ENT_MGR()
    {
        // TODO Auto-generated DoctrineORMTest->test__construct()
        $this->setExpectedException('\ImmutableStateStatusTracker\Exception\StorageAdapterException', DoctrineORM::EXC_ENTITY_MANAGER_NOT_DEFINED_IN_CONFIG_MESSAGE, DoctrineORM::EXC_ENTITY_MANAGER_NOT_DEFINED_IN_CONFIG_CODE);
        
        $this->doctrineORM->__construct(array(), $this->getServiceManager());
    }

    /**
     * Tests Disk->fetchJob()
     */
    public function testFetchJob()
    {
        $job = $this->doctrineORM->createJob(array(
            'first_component'
        ));
        $this->assertInstanceOf('ImmutableStateStatusTracker\Entity\Job', $job);
        
        $newJob = $this->doctrineORM->fetchJob($job->getJobId());
        
        $this->assertInstanceOf('\ImmutableStateStatusTracker\Entity\Job', $newJob);
    }

    /**
     * Tests Disk->removeAllStatusEvents()
     */
    public function testRemoveAllStatusEvents()
    {
        $job = $this->doctrineORM->createJob(array(
            'first_component'
        ));
        $this->assertInstanceOf('ImmutableStateStatusTracker\Entity\Job', $job);
        
        $event = $this->doctrineORM->addStatusEvent($job->getJobId(), $job->getComponents()[0], StatusEvent::STATUS_IN_PROGRESS, "Started event with unit test");
        
        $this->assertInstanceOf('\ImmutableStateStatusTracker\Entity\StatusEvent', $event);
        
        $this->doctrineORM->removeAllStatusEvents($job);
    }

    /**
     * Tests Disk->removeJob()
     */
    public function testRemoveJob()
    {
        $job = $this->doctrineORM->createJob(array(
            'first_component'
        ));
        $this->assertInstanceOf('ImmutableStateStatusTracker\Entity\Job', $job);
        
        $this->doctrineORM->removeJob($job);
        $repo = $this->getServiceManager()
            ->get('doctrine.entitymanager.orm_default')
            ->getRepository('ImmutableStateStatusTracker\StorageAdapter\Doctrine\Job');
        
        $noJob = $repo->find($job->getJobId());
        
        $this->assertNotInstanceOf('ImmutableStateStatusTracker\Entity\Job', $noJob);
    }

    /**
     * Tests Disk->fetchStatusEvents()
     */
    public function testFetchStatusEvents()
    {
        $job = $this->doctrineORM->createJob(array(
            'first_component'
        ));
        $this->assertInstanceOf('ImmutableStateStatusTracker\Entity\Job', $job);
        
        $event = $this->doctrineORM->addStatusEvent($job->getJobId(), $job->getComponents()[0], StatusEvent::STATUS_IN_PROGRESS, "Started event with unit test");
        
        $this->assertInstanceOf('\ImmutableStateStatusTracker\Entity\StatusEvent', $event);
        
        $events = $this->doctrineORM->fetchStatusEvents($job);
        
        $this->assertInternalType('array', $events);
        
        $this->assertArrayHasKey(0, $events);
        
        $this->assertInstanceOf('\ImmutableStateStatusTracker\Entity\StatusEvent', $events[0]);
    }

    /**
     * Tests Disk->createJob()
     */
    public function testCreateJob()
    {
        $job = $this->doctrineORM->createJob(array(
            'first_component'
        ));
        
        $this->assertInstanceOf('ImmutableStateStatusTracker\Entity\Job', $job);
    }

    /**
     * Tests Disk->addStatusEvent()
     */
    public function testAddStatusEvent()
    {
        $job = $this->doctrineORM->createJob(array(
            'first_component'
        ));
        $this->assertInstanceOf('ImmutableStateStatusTracker\Entity\Job', $job);
        
        $event = $this->doctrineORM->addStatusEvent($job->getJobId(), $job->getComponents()[0], StatusEvent::STATUS_IN_PROGRESS, "Started event with unit test");
        
        $this->assertInstanceOf('ImmutableStateStatusTracker\Entity\StatusEvent', $event);
    }

    public function testFetchAllJobs()
    {
        $pageNum = 1;
        
        for ($i = 0; $i < 99; $i ++) {
            
            $job = $this->doctrineORM->createJob(array(
                'first_component'
            ));
            unset($job);
        }
        
        $res = $this->doctrineORM->fetchAllJobs($pageNum);
        
        $this->assertEquals(50, $res->getCurrentItemCount());
        
        foreach ($res->getCurrentItems() as $key => $job) {
            $this->assertInstanceOf('ImmutableStateStatusTracker\Entity\Job', $job);
        }
        
        $this->assertGreaterThanOrEqual(2, $res->getPages()->pageCount);
    }

    /**
     * Tests Disk->removeOldJobs()
     */
    public function testRemoveOldJobs()
    {
        $time = new DateTime();
        
        $this->doctrineORM->removeOldJobs($time);
        // this should remove EVERYTHING
        
        /**
         *
         * @var
         *
         */
        $repo = $this->getServiceManager()
            ->get('doctrine.entitymanager.orm_default')
            ->getRepository('ImmutableStateStatusTracker\StorageAdapter\Doctrine\Job');
        
        $items = $repo->findAll();
        
        $this->assertLessThanOrEqual(2, count($items));
    }
}

