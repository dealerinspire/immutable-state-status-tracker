<?php
use ImmutableStateStatusTracker\Bootstrap;
use ImmutableStateStatusTracker\StorageAdapter\Disk;
use ImmutableStateStatusTracker\Entity\Job;
use ImmutableStateStatusTracker\Entity\StatusEvent;
require_once __DIR__ . '/../../../src/ImmutableStateStatusTracker/Exception/StorageAdapterException.php';
require_once __DIR__ . '/../../../src/ImmutableStateStatusTracker/StorageAdapterInterface.php';
require_once __DIR__ . '/../../../src/ImmutableStateStatusTracker/Entity/Job.php';
require_once __DIR__ . '/../../../src/ImmutableStateStatusTracker/Entity/StatusEvent.php';
require_once __DIR__ . '/../../../src/ImmutableStateStatusTracker/StorageAdapter/AbstractStorageAdapter.php';
require_once __DIR__ . '/../../../src/ImmutableStateStatusTracker/StorageAdapter/Disk.php';

/**
 * Disk test case.
 */
class DiskTest extends PHPUnit_Framework_TestCase
{

    private $path;

    /**
     *
     * @var Disk
     */
    private $disk;

    public function getServiceManager()
    {
        return Bootstrap::getServiceManager();
    }

    /**
     * Prepares the environment before running a test.
     */
    protected function setUp()
    {
        parent::setUp();
        
        // TODO Auto-generated DiskTest::setUp()
        
        $this->disk = new Disk(array(
            'path' => $this->path
        ), $this->getServiceManager());
        $this->assertInstanceOf('\ImmutableStateStatusTracker\StorageAdapterInterface', $this->disk);
    }

    /**
     * Cleans up the environment after running a test.
     */
    protected function tearDown()
    {
        // TODO Auto-generated DiskTest::tearDown()
        $this->disk = null;
        
        parent::tearDown();
    }

    /**
     * Constructs the test case.
     */
    public function __construct()
    {
        if (! is_dir(__DIR__ . "/../../data")) {
            mkdir(__DIR__ . "/../../data");
        }
        
        $this->path = realpath(__DIR__ . "/../../data");
    }

    /**
     * Tests Disk->fetchJob()
     */
    public function testFetchJob()
    {
        $job = $this->disk->createJob(array(
            'first_component'
        ));
        $this->assertFileExists($this->path . DIRECTORY_SEPARATOR . $job->getJobId() . '.json');
        
        $newJob = $this->disk->fetchJob($job->getJobId());
        
        $this->assertInstanceOf('\ImmutableStateStatusTracker\Entity\Job', $newJob);
    }

    /**
     * Tests Disk->removeAllStatusEvents()
     */
    public function testRemoveAllStatusEvents()
    {
        $job = $this->disk->createJob(array(
            'first_component'
        ));
        $this->assertFileExists($this->path . DIRECTORY_SEPARATOR . $job->getJobId() . '.json');
        
        $event = $this->disk->addStatusEvent($job->getJobId(), $job->getComponents()[0], StatusEvent::STATUS_IN_PROGRESS, "Started event with unit test");
        
        $this->assertFileExists($this->path . DIRECTORY_SEPARATOR . $job->getJobId() . DIRECTORY_SEPARATOR . $event->getEventId() . '.json');
        
        $this->disk->removeAllStatusEvents($job);
        
        $this->assertFileNotExists($this->path . DIRECTORY_SEPARATOR . $job->getJobId() . DIRECTORY_SEPARATOR . $event->getEventId() . '.json');
        
        $this->assertFileNotExists($this->path . DIRECTORY_SEPARATOR . $job->getJobId());
    }

    /**
     * Tests Disk->removeJob()
     */
    public function testRemoveJob()
    {
        $job = $this->disk->createJob(array(
            'first_component'
        ));
        $this->assertFileExists($this->path . DIRECTORY_SEPARATOR . $job->getJobId() . '.json');
        
        $this->disk->removeJob($job);
        $this->assertFileNotExists($this->path . DIRECTORY_SEPARATOR . $job->getJobId() . '.json');
    }

    /**
     * Tests Disk->fetchStatusEvents()
     */
    public function testFetchStatusEvents()
    {
        $job = $this->disk->createJob(array(
            'first_component'
        ));
        $this->assertFileExists($this->path . DIRECTORY_SEPARATOR . $job->getJobId() . '.json');
        
        $event = $this->disk->addStatusEvent($job->getJobId(), $job->getComponents()[0], StatusEvent::STATUS_IN_PROGRESS, "Started event with unit test");
        
        $this->assertFileExists($this->path . DIRECTORY_SEPARATOR . $job->getJobId() . DIRECTORY_SEPARATOR . $event->getEventId() . '.json');
        
        $events = $this->disk->fetchStatusEvents($job);
        
        $this->assertInternalType('array', $events);
        
        $this->assertArrayHasKey(0, $events);
        
        $this->assertInstanceOf('\ImmutableStateStatusTracker\Entity\StatusEvent', $events[0]);
    }

    /**
     * Tests Disk->createJob()
     */
    public function testCreateJob()
    {
        $job = $this->disk->createJob(array(
            'first_component'
        ));
        
        $this->assertFileExists($this->path . DIRECTORY_SEPARATOR . $job->getJobId() . '.json');
    }

    /**
     * Tests Disk->addStatusEvent()
     */
    public function testAddStatusEvent()
    {
        $job = $this->disk->createJob(array(
            'first_component'
        ));
        $this->assertFileExists($this->path . DIRECTORY_SEPARATOR . $job->getJobId() . '.json');
        
        $event = $this->disk->addStatusEvent($job->getJobId(), $job->getComponents()[0], StatusEvent::STATUS_IN_PROGRESS, "Started event with unit test");
        
        $this->assertFileExists($this->path . DIRECTORY_SEPARATOR . $job->getJobId() . DIRECTORY_SEPARATOR . $event->getEventId() . '.json');
    }

    public function testFetchAllJobs()
    {
        $pageNum = 1;
        
        for ($i = 0; $i < 99; $i ++) {
            
            $job = $this->disk->createJob(array(
                'first_component'
            ));
            unset($job);
        }
        
        $res = $this->disk->fetchAllJobs($pageNum);
        
        $this->assertEquals(50, $res->getCurrentItemCount());
        
        foreach ($res->getCurrentItems() as $key => $job) {
            $this->assertInstanceOf('ImmutableStateStatusTracker\Entity\Job', $job);
        }
        
        $this->assertGreaterThanOrEqual(2, $res->getPages()->pageCount);
    }

    /**
     * Tests Disk->removeOldJobs()
     */
    public function testRemoveOldJobs()
    {
        $time = new DateTime();
        
        $this->disk->removeOldJobs($time);
        
        $items = scandir($this->path);
        
        $this->assertLessThanOrEqual(2, count($items));
    }
}

