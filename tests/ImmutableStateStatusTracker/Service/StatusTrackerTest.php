<?php
use ImmutableStateStatusTracker\Bootstrap;
use ImmutableStateStatusTracker\Service\StatusTracker;
use ImmutableStateStatusTracker\StorageAdapter\Disk;
use ImmutableStateStatusTracker\Entity\StatusEvent;
use ImmutableStateStatusTracker\Entity\CalculatedJobStatus;
// Storage Adapter
require_once __DIR__ . '/../../../src/ImmutableStateStatusTracker/Exception/InvalidArgumentException.php';
require_once __DIR__ . '/../../../src/ImmutableStateStatusTracker/Exception/StorageAdapterException.php';
require_once __DIR__ . '/../../../src/ImmutableStateStatusTracker/StorageAdapterInterface.php';
require_once __DIR__ . '/../../../src/ImmutableStateStatusTracker/Entity/Job.php';
require_once __DIR__ . '/../../../src/ImmutableStateStatusTracker/Entity/StatusEvent.php';
require_once __DIR__ . '/../../../src/ImmutableStateStatusTracker/StorageAdapter/AbstractStorageAdapter.php';
require_once __DIR__ . '/../../../src/ImmutableStateStatusTracker/StorageAdapter/Disk.php';

// deps for the service itself
require_once __DIR__ . '/../../../src/ImmutableStateStatusTracker/Exception/CalculatingJobStatusException.php';
require_once __DIR__ . '/../../../src/ImmutableStateStatusTracker/Entity/CalculatedJobStatus.php';
require_once __DIR__ . '/../../../src/ImmutableStateStatusTracker/StatusTrackerServiceInterface.php';
require_once __DIR__ . '/../../../src/ImmutableStateStatusTracker/Service/StatusTracker.php';

/**
 * StatusTracker test case.
 */
class StatusTrackerTest extends PHPUnit_Framework_TestCase
{

    /**
     *
     * @var StatusTracker
     */
    private $statusTracker;

    private $path;

    public function getServiceManager()
    {
        return Bootstrap::getServiceManager();
    }

    /**
     * Prepares the environment before running a test.
     */
    protected function setUp()
    {
        parent::setUp();
        
        $storageAdapter = new Disk(array(
            'path' => $this->path
        ), $this->getServiceManager());
        
        $this->assertInstanceOf('\ImmutableStateStatusTracker\StorageAdapterInterface', $storageAdapter);
        
        // TODO Auto-generated StatusTrackerTest::setUp()
        
        $this->statusTracker = new StatusTracker($storageAdapter);
        
        $this->assertInstanceOf('\ImmutableStateStatusTracker\Service\StatusTracker', $this->statusTracker);
    }

    /**
     * Cleans up the environment after running a test.
     */
    protected function tearDown()
    {
        // TODO Auto-generated StatusTrackerTest::tearDown()
        $this->statusTracker = null;
        
        parent::tearDown();
    }

    /**
     * Constructs the test case.
     */
    public function __construct()
    {
        // TODO Auto-generated constructor
        $this->path = realpath(__DIR__ . "/../../data");
    }

    /**
     * Tests StatusTracker->calculateJobStatus()
     */
    public function testCalculateJobStatus()
    {
        $job = $this->statusTracker->createJob(array(
            'first_component'
        ));
        
        $status = $this->statusTracker->calculateJobStatus($job->getJobId());
        $this->assertEquals(CalculatedJobStatus::STATUS_QUEUED, $status->getOverallStatus());
        
        // let's add a status event to the job.
        $statusEvent1 = $this->statusTracker->addStatusEvent($job->getJobId(), 'first_component', StatusEvent::STATUS_IN_PROGRESS, "Event 1 started");
        $this->assertInstanceOf('ImmutableStateStatusTracker\Entity\StatusEvent', $statusEvent1);
        
        $statusAfterEvent1 = $this->statusTracker->calculateJobStatus($job->getJobId());
        $this->assertEquals(CalculatedJobStatus::STATUS_IN_PROGRESS, $statusAfterEvent1->getOverallStatus());
        
        // job completes
        
        $statusEvent2 = $this->statusTracker->addStatusEvent($job->getJobId(), 'first_component', StatusEvent::STATUS_COMPLETED, "Event 2 started");
        $this->assertInstanceOf('ImmutableStateStatusTracker\Entity\StatusEvent', $statusEvent2);
        
        $statusAfterEvent2 = $this->statusTracker->calculateJobStatus($job->getJobId());
        $this->assertEquals(CalculatedJobStatus::STATUS_COMPLETED, $statusAfterEvent2->getOverallStatus());
        
        // job fails
        $statusEvent3 = $this->statusTracker->addStatusEvent($job->getJobId(), 'first_component', StatusEvent::STATUS_FAILED, "Event 3 started");
        $this->assertInstanceOf('ImmutableStateStatusTracker\Entity\StatusEvent', $statusEvent3);
        
        $statusAfterEvent3 = $this->statusTracker->calculateJobStatus($job->getJobId());
        $this->assertEquals(CalculatedJobStatus::STATUS_FAILED, $statusAfterEvent3->getOverallStatus());
        
        $job2 = $this->statusTracker->createJob(array(
            'first_component',
            'second_component',
            'third_component'
        ));
        
        $j2status = $this->statusTracker->calculateJobStatus($job2->getJobId());
        $this->assertEquals(CalculatedJobStatus::STATUS_QUEUED, $j2status->getOverallStatus());
        
        // let's add a status event to the job.
        $j2statusEvent1 = $this->statusTracker->addStatusEvent($job2->getJobId(), 'first_component', StatusEvent::STATUS_IN_PROGRESS, "Event 1 started");
        $this->assertInstanceOf('ImmutableStateStatusTracker\Entity\StatusEvent', $j2statusEvent1);
        
        $j2statusAfterEvent1 = $this->statusTracker->calculateJobStatus($job2->getJobId());
        
        $this->assertEquals(CalculatedJobStatus::STATUS_IN_PROGRESS, $j2statusAfterEvent1->getOverallStatus());
        
        // job completes
        
        $j2statusEvent2 = $this->statusTracker->addStatusEvent($job2->getJobId(), 'first_component', StatusEvent::STATUS_FAILED, "individual component fails while in-progress");
        
        $j2statusAfterEvent2 = $this->statusTracker->calculateJobStatus($job2->getJobId());
        $this->assertEquals(CalculatedJobStatus::STATUS_FAILED_AND_STARTED, $j2statusAfterEvent2->getOverallStatus());
        
        $j2statusEvent2c = $this->statusTracker->addStatusEvent($job2->getJobId(), 'first_component', StatusEvent::STATUS_IN_PROGRESS, "");
        $j2statusEvent2c = $this->statusTracker->addStatusEvent($job2->getJobId(), 'first_component', StatusEvent::STATUS_COMPLETED, "completion");
        $j2statusEvent3 = $this->statusTracker->addStatusEvent($job2->getJobId(), 'second_component', StatusEvent::STATUS_COMPLETED, "more completion");
        $j2statusEvent4 = $this->statusTracker->addStatusEvent($job2->getJobId(), 'third_component', StatusEvent::STATUS_COMPLETED, "even more completion");
        $this->assertInstanceOf('ImmutableStateStatusTracker\Entity\StatusEvent', $j2statusEvent2);
        $this->assertInstanceOf('ImmutableStateStatusTracker\Entity\StatusEvent', $j2statusEvent3);
        $this->assertInstanceOf('ImmutableStateStatusTracker\Entity\StatusEvent', $j2statusEvent4);
        
        $j2statusAfterEvent4 = $this->statusTracker->calculateJobStatus($job2->getJobId());
        
        // var_dump($j2statusAfterEvent4);
        
        $this->assertEquals(CalculatedJobStatus::STATUS_COMPLETED, $j2statusAfterEvent4->getOverallStatus());
        
        $j2statusEvent5 = $this->statusTracker->addStatusEvent($job2->getJobId(), 'third_component', StatusEvent::STATUS_FAILED, "firing a single failure event after job coompletion");
        $j2statusAfterEvent5 = $this->statusTracker->calculateJobStatus($job2->getJobId());
        $this->assertEquals(CalculatedJobStatus::STATUS_FAILED, $j2statusAfterEvent5->getOverallStatus());
        
        // job fails
        $statusEvent5 = $this->statusTracker->addStatusEvent($job2->getJobId(), 'first_component', StatusEvent::STATUS_FAILED, "Event 3 started");
        $this->assertInstanceOf('ImmutableStateStatusTracker\Entity\StatusEvent', $statusEvent5);
        
        $statusAfterEvent5 = $this->statusTracker->calculateJobStatus($job2->getJobId());
        $this->assertEquals(CalculatedJobStatus::STATUS_FAILED, $statusAfterEvent5->getOverallStatus());
        // die();
    }

    /**
     * Tests StatusTracker->hasJob()
     */
    public function testHasJob()
    {
        // TODO Auto-generated StatusTrackerTest->testHasJob()
        // $this->markTestIncomplete("hasJob test not implemented");
        $failExample = $this->statusTracker->hasJob("MissingJob");
        
        $this->assertFalse($failExample);
        
        $shouldSucceed = $this->statusTracker->createJob(array(
            'first_component'
        ));
        
        $this->assertTrue($this->statusTracker->hasJob($shouldSucceed->getJobId()));
    }

    /**
     * Tests StatusTracker->createJob()
     */
    public function testCreateJob()
    {
        $res = $this->statusTracker->createJob(array(
            'first_component'
        ));
        
        $this->assertInstanceOf('ImmutableStateStatusTracker\Entity\Job', $res);
        
        $this->setExpectedException('ImmutableStateStatusTracker\Exception\InvalidArgumentException');
        $this->statusTracker->createJob(array());
    }

    /**
     * Tests StatusTracker->addStatusEvent()
     */
    public function testAddStatusEvent()
    {
        $job = $this->statusTracker->createJob(array(
            'first_component'
        ));
        
        $event = $this->statusTracker->addStatusEvent($job->getJobId(), $job->getComponents()[0], StatusEvent::STATUS_IN_PROGRESS, 'Started processing baby!');
        
        $this->assertInstanceOf('ImmutableStateStatusTracker\Entity\StatusEvent', $event);
    }

    /**
     * Tests StatusTracker->removeJob()
     */
    public function testRemoveJob()
    {
        $job = $this->statusTracker->createJob(array(
            'first_component'
        ));
        
        $this->statusTracker->removeJob($job->getJobId());
        
        $this->assertFalse($this->statusTracker->hasJob($job->getJobId()));
    }
}

